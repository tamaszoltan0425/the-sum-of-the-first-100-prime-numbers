package org.fasttrackit;

public class Main {

    public static final String FIRST100 = "The first one hundred prime numbers are: ";
    public static final String SUM = "The sum of the first one hundred prime numbers is: ";
    public static final String RED = "\u001B[31m";
    public static final String RESET = "\u001B[0m";

    public static final String CYAN = "\u001B[36m";

    public static void main(String[] args) {

        int primeCounter = 0;
        int sumOfPrime = 0;

        System.out.println("\n\t" + RED + FIRST100 + RESET);
        System.out.println();

        sumOfPrime = getSumOfPrime(primeCounter, sumOfPrime);

        System.out.println();
        System.out.println("\n\t" + RED + SUM + RESET + CYAN + sumOfPrime + RESET);
    }

    private static int getSumOfPrime(int primeCounter, int sumOfPrime) {
        for (int i = 2; primeCounter < 100; i++) {
            int counter = 0;
            for (int j = 2; j <= i / 2; j++) {
                if (i % j == 0) {
                    counter++;
                }
            }
            if (counter == 0) {
                System.out.print(CYAN + i + " " + RESET);
                primeCounter++;
                if (primeCounter == 50) {
                    System.out.println();
                }
                sumOfPrime = sumOfPrime + i;
            }
        }
        return sumOfPrime;
    }
}